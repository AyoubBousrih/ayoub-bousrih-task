import { TestService } from './test.service';
import { appRoutes } from './routerConfig';
import { ModalModule } from 'ngx-bootstrap';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DatePipe } from "@angular/common";


import { AppComponent } from './app.component';
import { PersonsComponent } from './components/persons/persons.component';

import {RouterModule } from '@angular/router';
import {HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ChartsComponent } from './components/charts/charts.component';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FilterPipe } from './filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    PersonsComponent,
    ChartsComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    FormsModule,
    ChartsModule,
    Ng2SearchPipeModule
  ],
  providers: [TestService,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
