import { ChartsComponent } from './components/charts/charts.component';

import { PersonsComponent } from './components/persons/persons.component';


import { Routes } from '@angular/router';

export const appRoutes: Routes = [
   
    
    {
        path: '',
        component: PersonsComponent
    },
    {
        path: 'charts',
        component: ChartsComponent
    },
    {
        path: 'persons',
        component: PersonsComponent
    }

];
