import { Person } from './model/person.model';
import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import { map } from "rxjs/operators";
@Injectable()
export class TestService {
  constructor(private http:Http) {
  }
  getPersons() {
    return this.http.get('https://test-frontend-neppo.herokuapp.com/pessoas.json')
    .pipe(map((res:Response) => res.json()));
}

  createPerson(person) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(person);
    return this.http.post('https://test-frontend-neppo.herokuapp.com/pessoas', body, options )
    //.pipe(map((res: Response) => res.json()));
  }
  updatePerson(person) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(person);
    console.log(person.id);
    return this.http.put('https://test-frontend-neppo.herokuapp.com/pessoas/' + person.id, body, options )
    //.pipe(map((res: Response) => res.json()));
  }
  deletePerson(person) {
    return this.http.delete('https://test-frontend-neppo.herokuapp.com/pessoas/' + person.id);
  }

  
}
