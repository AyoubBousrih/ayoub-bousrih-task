import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(persons: any, term: any): any {
    //check if search term is undefined
    if(term === undefined) return persons;
    //return updated persons
    return persons.filter(function(person){
      return person.nome.toLowerCase().includes(term.toLowerCase());
    })
  }

}
