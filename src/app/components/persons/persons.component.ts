import { Person } from "./../../model/person.model";
import { TestService } from "./../../test.service";
import { Component, TemplateRef } from "@angular/core";
import { Http } from "@angular/http";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";
import { DatePipe } from "@angular/common";



@Component({
  selector: "app-persons",
  templateUrl: "./persons.component.html",
  styleUrls: ["./persons.component.css"],
  
})
export class PersonsComponent {
  persons: any[];
  person: any;
  hidden = true;
  editPerson: Person;
  modalRef: BsModalRef;
  addPerson: Person;
  private url = "https://test-frontend-neppo.herokuapp.com/pessoas.json";

  constructor(
    private datePipe:DatePipe,
    private http: Http,
    private modalService: BsModalService,
    private _testService: TestService
  ) {
    http.get(this.url).subscribe(response => {
      this.persons = response.json();
    });
  }


  openModal(addTemplate: TemplateRef<any>) {
    this.modalRef = this.modalService.show(addTemplate);
  }

  openEditModal(editTemplate: TemplateRef<any>, person: Person) {
    this.modalRef = this.modalService.show(editTemplate);
    //this.editPerson = new Person();
    this.editPerson = person;
    console.log(person);
  }
  onSubmitEdit(form: NgForm, editTemplate: TemplateRef<any>) {
    this.modalRef.hide();
    this._testService.updatePerson(this.editPerson).subscribe(data => {
      console.log(this.editPerson);
      this.getPersons();
      this.editPerson = null;
    });
  }

  onSubmit(form: NgForm, addTemplate: TemplateRef<any>) {
    console.log(JSON.stringify(form.value));
    let person: Person = {
      nome: form.value.nome,
      dataNascimento: form.value.dataNascimento,
      identificacao: form.value.identificacao,
      sexo: form.value.sexo,
      endereco: form.value.endereco
    };
    this.modalRef.hide();

    
    this._testService.createPerson(person).subscribe(res => {
      alert(res);
      this.getPersons();
      form.reset();
      this.modalRef.hide();
      console.log(person);
    });

  }

  getPersons() {
    this._testService.getPersons().subscribe(
      // the first argument is a function which runs on success
      data => {
        this.persons = data;
      },
      // the second argument is a function which runs on error
      err => console.error(err),
      // the third argument is a function which runs on completion
      () => console.log("done loading persons")
    );
  }

  createPerson(name) {
    let person = { name: name };
    this._testService.createPerson(person).subscribe(
      data => {
        // refresh the list
        this.getPersons();
        return true;
      },
      error => {
        console.error("Error saving person!");
        return Observable.throw(error);
      }
    );
  }

  updatePerson(person) {
    this._testService.updatePerson(person).subscribe(
      data => {
        // refresh the list
        this.getPersons();
        return true;
      },
      error => {
        console.error("Error saving person!");
        return Observable.throw(error);
      }
    );
  }
  private dateChanged(newDate) {
    
    this.editPerson.dataNascimento= this.datePipe.transform(new Date(newDate),'yyyy-MM-dd');
    console.log(this.editPerson.dataNascimento); // <-- for testing
  }

  

  deletePerson(person) {
    if (confirm("Are you sure you want to delete " + person.nome + "?")) {
      this._testService.deletePerson(person).subscribe(
        data => {
          // refresh the list
          return true;
          console.log("delete test");
        },

        error => {
          this.getPersons();
        }
      );
    }
  }
  
}
