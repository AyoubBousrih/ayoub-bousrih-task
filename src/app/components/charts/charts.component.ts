import { Person } from './../../model/person.model';
import { TestService } from './../../test.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit{
  persons:Person[]=[];
  show:boolean = false;
  ngOnInit(){
    this._testService.getPersons().subscribe(data=>{
      console.log("age");
      this.persons=data;
      for(let i=0;i<this.persons.length;i++){
        let j = this.calculAge(this.persons[i].dataNascimento);
        //console.log(j);
        if (j<10){
          this.pieChartData[0]++;
        }else if (j<20){
          this.pieChartData[1]++;
        }else if (j<30){
          this.pieChartData[2]++;

        }else if (j<40){
          this.pieChartData[3]++;
          
        }else{
          this.pieChartData[4]++;

        }
        
        let gender= this.persons[i].sexo;
        if (gender == "male")
          this.pieChartData1[0]++;
        else if (gender == "female")
          this.pieChartData1[1]++;
      }
      this.show=true;
    })
  }

  // Pie
  public pieChartLabels:string[] = ['0-9', '10-19', '20-29', '30-39', '40 +'];
  public pieChartData:number[] = [0, 0, 0, 0, 0];
  public pieChartType:string = 'pie';
  //Pie-M/F
  public pieChartLabels1:string[] = ['Male', 'Female'];
  public pieChartData1:number[] = [0,0];
  public pieChartType1:string = 'pie1';

  constructor(private _testService:TestService){

  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  private calculAge(date:string):number{ 
    let dateb = new Date(date);
    var timeDiff = Math.abs(Date.now() - dateb.getTime());
    return Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
  }



}
